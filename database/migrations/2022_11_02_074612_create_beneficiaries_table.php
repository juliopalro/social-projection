<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiaries', function (Blueprint $table) {
            $table->id();
            $table->string('full_name'); // APELLIDOS Y NOMBRES
            $table->string('dni', 10)->unique(); // DNI
            $table->date('date_of_birth'); // FECHA DE NACIMIENTO
            $table->date('date_of_issue'); // FECHA DE EMISION
            $table->string('address'); // DIRECCION
            $table->integer('family_integrants'); // INTEGRANTES DE LA FAMILIA
            $table->string('zone'); // ZONA
            $table->string('cell_phone'); // CELULAR
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiaries');
    }
}
