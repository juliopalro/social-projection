<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BeneficiaryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'full_name'=> $this->faker->name(),
            'dni'=> $this->faker->unique()->dni(),
            'date_of_birth'=> $this->faker->date(),
            'date_of_issue'=> $this->faker->date(),
            'address'=> $this->faker->address(),
            'family_integrants'=> $this->faker->numberBetween(2, 20),
            'zone'=> $this->faker->randomElement(['zona1', 'zona2', 'zona3', 'zona4', 'zona5']),
            'cell_phone'=> $this->faker->numberBetween(10000000, 99999999)
        ];
    }
}
