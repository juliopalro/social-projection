<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BeneficiaryController;
use App\Http\Controllers\ReportsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('index');

Route::middleware(['auth'])->group(function(){
    // CRUD Beneficiarios
    Route::resource('/beneficiario', BeneficiaryController::class);

    //Reportes
    Route::get('/reporte-zona', [ReportsController::class, 'report_consult_zona'])->name('report_consult_zona');
    Route::post('/reporte-zona', [ReportsController::class, 'report_view_zona'])->name('report_view_zona');
});
