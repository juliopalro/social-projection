<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Beneficiary;

class ReportsController extends Controller
{
    public function report_consult_zona()
    {
        return view('reports.zona');
    }

    public function report_view_zona(Request $request)
    {
        $beneficiaries = Beneficiary::where('zone', $request->zone)->get();
        return view('reports.zona')->with('beneficiaries', $beneficiaries);
    }
}
