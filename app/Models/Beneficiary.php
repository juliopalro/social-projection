<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'dni',
        'date_of_birth',
        'date_of_issue',
        'address',
        'family_integrants',
        'zone',
        'cell_phone'
    ];

    protected $dates = [
        'date_of_birth',
        'date_of_issue',
        'crated_at'
    ];
}
