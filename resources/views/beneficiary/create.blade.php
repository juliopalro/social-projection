@extends('layouts.app')

@section('content')

<div class="container">
    <h1 class="text-primary">Creación de nuevo beneficiario</h1>

    <form method="POST" action="{{ route('beneficiario.store') }}">
        @csrf

        <div class="mb-3">
          <label for="full_name" class="col-form-label">Nombre completo</label>
          <input type="text" class="form-control @error('full_name') is-invalid @enderror" id="full_name" name="full_name"
            placeholder="Pablito Calvo" value="{{ old('full_name') }}">
          @error('full_name')
            <div class="invalid-feedback">{{ $message }}</div>
          @enderror
        </div>

        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                  <label for="dni" class="col-form-label">DNI</label>
                  <input type="text" class="form-control @error('dni') is-invalid @enderror" id="dni" name="dni"
                    placeholder="25635689" value="{{ old('dni') }}">
                  @error('dni')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                  <label for="date_of_birth" class="col-form-label">Fecha de nacimiento</label>
                  <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" id="date_of_birth" name="date_of_birth" value="{{ old('date_of_birth') }}">
                  @error('date_of_birth')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                  <label for="date_of_issue" class="col-form-label">Fecha de emisión</label>
                  <input type="date" class="form-control @error('date_of_issue') is-invalid @enderror" id="date_of_issue" name="date_of_issue" value="{{ old('date_of_issue') }}">
                  @error('date_of_issue')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
            </div>
        </div>

        <div class="mb-3">
          <label for="address" class="col-form-label">Dirección</label>
          <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address"
            placeholder="Pablito Calvo" value="{{ old('address') }}">
          @error('address')
            <div class="invalid-feedback">{{ $message }}</div>
          @enderror
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="mb-3">
                  <label for="family_integrants" class="col-form-label">N° de familiares</label>
                  <input type="number" class="form-control @error('family_integrants') is-invalid @enderror" id="family_integrants" name="family_integrants"
                    placeholder="5" value="{{ old('family_integrants') }}">
                  @error('family_integrants')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
            </div>
            <div class="col-sm-4">
                <div class="mb-3">
                  <label for="zone" class="col-form-label">Zona</label>
                  <select class="form-select" name="zone" id="zone" required>
                      <option value="zona1" {{ old('zone') == 'zona1' ? 'selected': '' }}>zona 1</option>
                      <option value="zona2" {{ old('zone') == 'zona2' ? 'selected': '' }}>zona 2</option>
                      <option value="zona3" {{ old('zone') == 'zona3' ? 'selected': '' }}>zona 3</option>
                      <option value="zona4" {{ old('zone') == 'zona4' ? 'selected': '' }}>zona 4</option>
                      <option value="zona5" {{ old('zone') == 'zona5' ? 'selected': '' }}>zona 5</option>
                  </select>
                  @error('zone')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
            </div>
            <div class="col-sm-4">
                <div class="mb-3">
                  <label for="cell_phone" class="col-form-label">N° telefónco</label>
                  <input type="text" class="form-control @error('cell_phone') is-invalid @enderror" id="cell_phone" name="cell_phone"
                    placeholder="964563256" value="{{ old('cell_phone') }}">
                  @error('cell_phone')
                    <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
            </div>
        </div>

        <div class="col-auto mt-3">
            <button type="submit" class="btn btn-primary mb-3">Registrar</button>
            <a class="btn btn-default mb-3" href="{{ route('beneficiario.index') }}">Cancelar</a>
        </div>
    </form>
</div>
@endsection