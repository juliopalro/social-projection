@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Lista de beneficiarios registrados</h1>

    <table class="table">
        <thead class="table-dark">
            <tr>
                <th>Nombre completo</th>
                <th>DNI</th>
                <th>Fecha de nacimiento</th>
                <th>Fecha de emisión</th>
                <th>Fecha de registro</th>
                <th>Dirección</th>
                <th>N° familiares</th>
                <th>Zona</th>
                <th>Teléfono</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($beneficiaries as $beneficiary)
            <tr>
                <td>{{ $beneficiary->full_name }}</td>
                <td>{{ $beneficiary->dni }}</td>
                <td>{{ $beneficiary->date_of_birth->format('d-m-Y') }}</td>
                <td>{{ $beneficiary->date_of_issue->format('d-m-Y') }}</td>
                <td>{{ $beneficiary->created_at->format('d-m-Y') }}</td>
                <td>{{ $beneficiary->address }}</td>
                <td>{{ $beneficiary->family_integrants }}</td>
                <td>{{ $beneficiary->zone }}</td>
                <td>{{ $beneficiary->cell_phone }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection