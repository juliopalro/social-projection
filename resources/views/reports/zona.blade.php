@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" class="mb-3" action="{{ route('report_view_zona') }}">
            @csrf

            <div class="row">
                <div class="col-sm-6">
                    <select class="form-select" name="zone" id="zone" required>
                        <option value="zona1" {{ old('zone') == 'zona1' ? 'selected': '' }}>zona 1</option>
                        <option value="zona2" {{ old('zone') == 'zona2' ? 'selected': '' }}>zona 2</option>
                        <option value="zona3" {{ old('zone') == 'zona3' ? 'selected': '' }}>zona 3</option>
                        <option value="zona4" {{ old('zone') == 'zona4' ? 'selected': '' }}>zona 4</option>
                        <option value="zona5" {{ old('zone') == 'zona5' ? 'selected': '' }}>zona 5</option>
                    </select>
                      @error('zone')
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary mb-3">Consultar</button>
                </div>
            </div>
        </form>

        @isset($beneficiaries)

            <table class="table">
                <thead class="table-dark">
                    <tr>
                        <th>Nombre completo</th>
                        <th>DNI</th>
                        <th>Fecha de nacimiento</th>
                        <th>Fecha de emisión</th>
                        <th>Fecha de registro</th>
                        <th>Dirección</th>
                        <th>N° familiares</th>
                        <th>Zona</th>
                        <th>Teléfono</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($beneficiaries as $beneficiary)
                    <tr>
                        <td>{{ $beneficiary->full_name }}</td>
                        <td>{{ $beneficiary->dni }}</td>
                        <td>{{ $beneficiary->date_of_birth->format('d-m-Y') }}</td>
                        <td>{{ $beneficiary->date_of_issue->format('d-m-Y') }}</td>
                        <td>{{ $beneficiary->created_at->format('d-m-Y') }}</td>
                        <td>{{ $beneficiary->address }}</td>
                        <td>{{ $beneficiary->family_integrants }}</td>
                        <td>{{ $beneficiary->zone }}</td>
                        <td>{{ $beneficiary->cell_phone }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @endisset

    </div>
@endsection